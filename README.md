# package-scope

Adds and removes scope from package.json name to deal with this [NPM issue](https://github.com/npm/npm/pull/19033)

## Usage

```
npm install @glint-ui/package-scope --save-dev
```

Add `@glint-ui` scope to package.json (and dist/package.json if it exists).

```
package-json add @glint-ui
```

Remove `@glint-ui` scope from package.json

```
package-json remove @glint-ui
```
