#!/usr/bin/env node
'use strict'

const errMsg = "Expecting 'package-scope <add|remove> @scope-name'\n\n";

if (process.argv.length < 4) {
    throw new Error(`Missing arguments.\n${errMsg}`)
}

const fs = require('fs')
const cmd = process.argv[2]
const scope = process.argv[3]

function updateScope (cmd, scope, path, beautifyJson) {
    
    const absPath = `${process.cwd()}/${path}`
    let pkg
    let name
    
    try {
        pkg = require(absPath)
    } catch (e) {
        return console.log(`Skipping ${path}, file not found.`)
    }
    
    switch (cmd) {
        case 'add':
            if (pkg.name.match(scope)) {
                return console.log(`Skipping ${path}, '${scope}' already exists.`)
            }
            name = `${scope}/${pkg.name}`
            break
        case 'remove':
            if (!pkg.name.match(scope)) {
                return console.log(`Skipping ${path}, '${scope}' not found.`)
            }
            name = pkg.name.replace(`${scope}/`, '')
            break
        default:
            throw new Error("cmd must be 'add' or 'remove'")
            break
    }
    
    pkg.name = name
    
    if (beautifyJson) {
        console.log(`Updating ${path}`)
        fs.writeFileSync(absPath, JSON.stringify(pkg, null, 2), 'utf8')
    } else {
        fs.writeFileSync(absPath, JSON.stringify(pkg), 'utf8')
    }
}

switch (cmd) {
    case 'add':
        updateScope(cmd, scope, 'package.json', true)
        updateScope(cmd, scope, 'dist/package.json')
        break
    case 'remove':
        updateScope(cmd, scope, 'package.json', true)
        break
    default:
        throw new Error(`Wrong arguments.\n${errMsg}`)
}